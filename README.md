# Llistat de links
### P0
[Part inicial](https://andreitasg.gitlab.io/pelim4/P0/AA1.1.3a.html)
### P1
[Part amb css](https://andreitasg.gitlab.io/pelim4/P1/AA1.1.3a.html)
### P2
[Part d'integració plantilla responsive](https://andreitasg.gitlab.io/pelim4/P2/index.html)
### P3
[Part de boostrap](https://andreitasg.gitlab.io/pelim4/P3/index.html)
# Llistat de propietats
1. text-align
2. display
3. flex-wrap
4. margin
5. float
6. color
7. background-image
8. background-attachment
9. padding-top
10. padding-bottom
11. padding-left
12. padding-right
13. background-color
14. font-family
15. opacity
16. background
17. widht
18. height
19. font-size
20. border-bottom-style
21. margin-right

